#!/bin/sh
# Continuously read a GPIO pin until Ctrl-C is used.
# Reads are done 1 second apart.
# -------------------------------------------------

pin=$1
while [ 1 ]
do
    state=`./imgpio -r -p $pin`
    if [ $state -eq 0 ]
    then
        echo "Pin $pin is set"
    else
        echo "Pin $pin is not set"
    fi
    sleep 1
done
