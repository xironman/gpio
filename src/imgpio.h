/*******************************************************************************
 * imgpio
 *
 * imgpio.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef IMGPIO_H
#define IMGPIO_H

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef IMGPIO_C
int     daemonEnabled = 0;
char    errBuf[256];
#else
extern int      daemonEnabled;
extern char     errBuf[];
#endif /* IMGPIO_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "imgpio"
#define MAXBUF      4096

// Where the config file is located
#define F_CFG   "/etc/imgpio.cfg"
#define F_CFG_T "data/imgpio.cfg"

// GPIO Paths
#define GPIO_SYSFS_PATH     "/sys/class/gpio"
#define GPIO_EXPORT_PATH    GPIO_SYSFS_PATH"/export"
#define GPIO_UNEXPORT_PATH  GPIO_SYSFS_PATH"/unexport"

// Size of buffers used for access to sysfs 
#define BUFFER_MAX      4
#define DIRECTION_MAX   35
#define VALUE_MAX       30

// Direction of the GPIO pin
#define GPIO_IN         0
#define GPIO_OUT        1
#define GPIO_IN_STR     "in"
#define GPIO_OUT_STR    "out"
 
// On / Off definitions
#define GPIO_LOW  0
#define GPIO_HIGH 1
 
// Which pins are read and which are write.
#define PIN  24 /* P1-18 */
#define POUT 4  /* P1-07 */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef IMGPIO_C
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include "cli.h"

#endif /* !IMGPIO_H */

