/*******************************************************************************
 * imgpio
 *
 * imgpio.c:  Use sysfs to read/write GPIO pins.
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define IMGPIO_C

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pibox/utils.h>

#include "imgpio.h"

/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig, siginfo_t *si, void *uc )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * siginfo_t *si    Structure that includes timer over run information and timer ID
 * void      *uc    Unused but required with sigaction handlers.
 * ========================================================================
 */
static void
sigHandler( int sig, siginfo_t *si, void *uc )
{
    switch (sig)
    {
        case SIGTERM:
            /* Bring it down gracefully. */
            daemonEnabled = 0;
            break;
    }
}

/*
 * ========================================================================
 * Name:   gpioEnable
 * Prototype:  static void gpioEnable( int pin )
 *
 * Description:
 * Enables the specified GPIO pin.  The pin number must be the Raspberry Pi
 * designated GPIO number, not the physical pin on the board.
 *
 * Input Arguments:
 * int       pin    Enable the specified pin for I/O
 *
 * Returns:
 * 0 on success.
 * -1 on failure.
 * ========================================================================
 */
static int
gpioEnable( int pin )
{
    char buffer[BUFFER_MAX];
    ssize_t bytes_written;
    int fd;
 
    fd = open(GPIO_EXPORT_PATH, O_WRONLY);
    if (-1 == fd) 
    {
        piboxLogger(LOG_ERROR, "Failed to open %s to enable: %s\n", GPIO_EXPORT_PATH, strerror(errno));
        return(-1);
    }
 
    bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
    write(fd, buffer, bytes_written);
    close(fd);
    return(0);
}

/*
 * ========================================================================
 * Name:   gpioDisable
 * Prototype:  static void gpioDisable( int pin )
 *
 * Description:
 * Disable the specified GPIO pin.  The pin number must be the Raspberry Pi
 * designated GPIO number, not the physical pin on the board.
 *
 * Input Arguments:
 * int       pin    Disable the specified pin from I/O
 *
 * Returns:
 * 0 on success.
 * -1 on failure.
 * ========================================================================
 */
static int
gpioDisable( int pin )
{
    char buffer[BUFFER_MAX];
    ssize_t bytes_written;
    int fd;
 
    fd = open(GPIO_UNEXPORT_PATH, O_WRONLY);
    if (-1 == fd) 
    {
        piboxLogger(LOG_ERROR, "Failed to open %s to disable: %s\n", GPIO_UNEXPORT_PATH, strerror(errno));
        return(-1);
    }
 
    bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
    write(fd, buffer, bytes_written);
    close(fd);
    return(0);
}

/*
 * ========================================================================
 * Name:   gpioDirection
 * Prototype:  static void gpioDirection( int pin, int direction )
 *
 * Description:
 * Disable the specified GPIO pin.  The pin number must be the Raspberry Pi
 * designated GPIO number, not the physical pin on the board.
 *
 * Input Arguments:
 * int       pin        Disable the specified pin from I/O
 * int       direction  GPIO_IN for reading, GPIO_OUT for writing
 *
 * Returns:
 * 0 on success.
 * -1 on failure.
 * ========================================================================
 */
static int
gpioDirection( int pin, int direction )
{
    char path[DIRECTION_MAX];
    char buffer[BUFFER_MAX];
    int fd;
 
    snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
    fd = open(path, O_WRONLY);
    if (-1 == fd) 
    {
        piboxLogger(LOG_ERROR, "Failed to open %s for writing!\n", path, strerror(errno));
        return(-1);
    }
 
    sprintf(buffer, "%s", (GPIO_IN == direction) ? GPIO_IN_STR : GPIO_OUT_STR);
    if (-1 == write(fd, buffer, strlen(buffer) ) )
    {
        piboxLogger(LOG_ERROR, "Failed to set direction!\n");
        close(fd);
        return(-1);
    }
 
    close(fd);
    return(0);
}

/*
 * ========================================================================
 * Name:   gpioRead
 * Prototype:  static void gpioRead( int pin )
 *
 * Description:
 * Read the state of the specified pin.
 *
 * Input Arguments:
 * int       pin        Disable the specified pin from I/O
 *
 * Returns:
 * GPIO_HIGH or GPIO_LOW on successful read.
 * -1 on failed read.
 * ========================================================================
 */
static int
gpioRead( int pin )
{
    char path[VALUE_MAX];
    char value_str[3];
    int fd;
 
    snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
    fd = open(path, O_RDONLY);
    if (-1 == fd) 
    {
        piboxLogger(LOG_ERROR, "Failed to open %s for reading!\n", path, strerror(errno));
        return(-1);
    }
 
    if (-1 == read(fd, value_str, 3)) 
    {
        piboxLogger(LOG_ERROR, "Failed to read value!\n");
        close(fd);
        return(-1);
    }
 
    close(fd);
    return(atoi(value_str));
}

/*
 * ========================================================================
 * Name:   gpioWrite
 * Prototype:  static void gpioWrite( int pin, int state )
 *
 * Description:
 * Write the state to the specified pin.
 *
 * Input Arguments:
 * int       pin        Disable the specified pin from I/O
 * int       state      GPIO_HIGH or GPIO_LOW
 *
 * Returns:
 * GPIO_HIGH or GPIO_LOW on successful write.
 * -1 on failed write.
 * ========================================================================
 */
static int
gpioWrite( int pin, int state )
{
    char path[VALUE_MAX];
    char value_str[BUFFER_MAX];
    int fd;
    char value[BUFFER_MAX];

    if ( (GPIO_HIGH != state) && (GPIO_LOW != state) )
    {
        piboxLogger(LOG_ERROR, "Invalid GPIO value (%d) for writing!\n", state);
        return(-1);
    }
 
    snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
    fd = open(path, O_WRONLY);
    if (-1 == fd) 
    {
        piboxLogger(LOG_ERROR, "Failed to open gpio value for writing: %s\n", strerror(errno));
        return(-1);
    }
 
    sprintf(value, "%d", (GPIO_HIGH == state) ? 1 : 0);
    if (1 != write(fd, value, 1)) 
    {
        piboxLogger(LOG_ERROR, "Failed to write value!\n");
        close(fd);
        return(-1);
    }
 
    close(fd);
    return(0);
}

/*
 * ========================================================================
 * Name:   main
 *
 * Description:
 * Setup the specified pin and access.
 *
 * Returns:
 * READ mode: 0 or 1 on success.
 * WRITE mode: 0 on success.
 * Otherwise:
 * -1 if pin cannot be setup for access.
 * -2 if the pin cannot be read or written.
 *
 * Notes:
 * This is a C program to read and write GPIO pins on the Raspberry Pi.
 * It uses the sysfs filesystem to directly access the pins.
 * See http://elinux.org/RPi_GPIO_Code_Samples#sysfs
 * ========================================================================
 */
int
main(int argc, char *argv[])
{
    int value;
    int rc = 0;

    /* Load saved configuration and parse command line */
    initConfig();
    parseArgs(argc, argv);
    validateConfig();

    /* Setup logging */
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);

    if ( cliOptions.logFile != NULL )
    {
        piboxLogger(LOG_INFO, "Log file: %s\n", cliOptions.logFile);
    }
    else
    {
        piboxLogger(LOG_INFO, "No log file configured.\n");
    }

    /* If requested, disable the pin and exit. */
    if ( isCLIFlagSet( CLI_DISABLE ) )
    {
        goto gpio_cleanup;
    }

    /* Enable the pin */
    piboxLogger(LOG_INFO, "Enabling pin %d\n", cliOptions.pin);
    if (-1 == gpioEnable(cliOptions.pin) )
    {
        rc = -1;
        goto imgpio_exit;
    }

    /* Set the direction for the pin and perform the action */
    if ( isCLIFlagSet( CLI_READ ) )
    {
        piboxLogger(LOG_INFO, "Setting mode to READ\n");
        if (-1 == gpioDirection(cliOptions.pin, GPIO_IN) )
        {
            rc = -1;
            goto gpio_cleanup;
        }
        piboxLogger(LOG_INFO, "Reading pin %d\n", cliOptions.pin);
        value = gpioRead(cliOptions.pin);
        if ( value < 0 )
        {
            rc = -2;
            goto gpio_cleanup;
        }
        else
        {
            printf("%d\n", gpioRead(cliOptions.pin));
            goto imgpio_exit;
        }
    }
    else
    {
        piboxLogger(LOG_INFO, "Setting mode to WRITE\n");
        if (-1 == gpioDirection(cliOptions.pin, GPIO_OUT) )
        {
            rc = -1;
            goto gpio_cleanup;
        }
        piboxLogger(LOG_INFO, "Writing %d to pin %d\n", cliOptions.value, cliOptions.pin);
        if (-1 == gpioWrite(cliOptions.pin, cliOptions.value))
        {
            rc = -2;
            goto gpio_cleanup;
        }
        // Don't disable pin if we just wrote to it.
        goto imgpio_exit;
    }

    /* Disable the pin */
gpio_cleanup:
    piboxLogger(LOG_INFO, "Disabling pin %d\n", cliOptions.pin);
    if (-1 == gpioDisable(cliOptions.pin) )
        rc = -1;

    /* Cleanup */
imgpio_exit:
    piboxLoggerShutdown();
    return(rc);
}
