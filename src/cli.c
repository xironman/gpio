/*******************************************************************************
 * imgpio
 *
 * cli.c:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CLI_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sched.h>
#include <sys/mman.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <pibox/utils.h>

#include "imgpio.h"

static pthread_mutex_t cliOptionsMutex = PTHREAD_MUTEX_INITIALIZER;

/*========================================================================
 * Name:   parseArgs
 * Prototype:  void parseArgs( int, char ** )
 *
 * Description:
 * Parse the command line
 *
 * Input Arguments:
 * int argc         Number of command line arguments
 * char **argv      Command line arguments to parse
 *========================================================================*/
void
parseArgs(int argc, char **argv)
{
    int opt;

    /* Suppress error messages from getopt_long() for unrecognized options. */
    opterr = 0;

    /* Parse the command line. */
    while ( (opt = getopt(argc, argv, CLIARGS)) != -1 )
    {
        switch (opt)
        {
            /* -p: The pin of interest */
            case 'p':
                cliOptions.pin = atoi(optarg);
                break;

            /* Disable pin. */
            case 'd':
                cliOptions.flags |= CLI_DISABLE;
                break;

            /* Enable reading from pin. */
            case 'r':
                cliOptions.flags |= CLI_READ;
                break;

            /* Enable reading from writing and save value. */
            case 'w':
                cliOptions.flags |= CLI_WRITE;
                cliOptions.value = atoi(optarg);
                break;

            /* Enable logging to local file. */
            case 'l':
                cliOptions.flags |= CLI_LOGTOFILE;
                cliOptions.logFile = g_strdup(optarg);
                break;

            /* -v: Verbose output (verbose is a library variable). */
            case 'v':
                cliOptions.verbose = atoi(optarg);
                break;

            default:
                printf("%s\nVersion: %s - %s\n", PROG, VERSTR, VERDATE);
                printf(USAGE);
                exit(0);
                break;
        }
    }
}

/*========================================================================
 * Name:   initConfig
 * Prototype:  void initConfig( void )
 *
 * Description:
 * Initialize run time configuration.  These are the default
 * settings.
 *========================================================================*/
void
initConfig( void )
{
    memset(&cliOptions, 0, sizeof(CLI_T));
    cliOptions.pin = -1;

    /* Are we running as root? */
    if ( getuid() == 0 )
        cliOptions.flags |= CLI_ROOT;
}

/*========================================================================
 * Name:   validateConfig
 * Prototype:  void validateConfig( void )
 *
 * Description:
 * Validate configuration file used as input
 *========================================================================*/
void
validateConfig( void )
{
    if ( !isCLIFlagSet( CLI_DISABLE ) &&
         !isCLIFlagSet( CLI_READ ) &&
         !isCLIFlagSet( CLI_WRITE ) )
    {
        fprintf(stderr, "You must specify either -d, -r or -w.");
        exit(1);
    }

    /* 
     * Validate pin value.
     * These are valid only for Models A, A+, B, B+, 2B, 3B, and Zero.
     * GPIO pins 0-25 are supported, but use at your own risk.
     */
    if( (cliOptions.pin <0) || (cliOptions.pin >30) )
    {
        fprintf(stderr, "Invalid value for pin.");
        exit(1);
    }

    /* Validate write value */
    if ( cliOptions.flags & CLI_WRITE )
    {
        switch( cliOptions.value )
        {
            case 0:
            case 1:
                break;
            default:
                fprintf(stderr, "Invalid value for write mode.");
                exit(1);
        }
    }
}

/*========================================================================
 * Name:   isCLIFlagSet
 * Prototype:  void isCLIFlagSet( int )
 *
 * Description:
 * Checks to see if an option is set in cliOptions.flags using a thread lock.
 * 
 * Returns;
 * 0 if requested flag is not set.
 * 1 if requested flag is set.
 *========================================================================*/
int
isCLIFlagSet( int bits )
{
    int status = 0;

    if ( cliOptions.flags & bits )
        status = 1;

    return status;
}

/*========================================================================
 * Name:   setCLIFlag
 * Prototype:  void setCLIFlag( int )
 *
 * Description:
 * Set options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
setCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags |= bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

/*========================================================================
 * Name:   unsetCLIFlag
 * Prototype:  void unsetCLIFlag( int )
 *
 * Description:
 * Unset options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
unsetCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags &= ~bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}
